/*	Copyright 2012 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#ifndef LISTSORT_H_
#define LISTSORT_H_

#include <limits.h>
#include <stddef.h>

struct listsort_vtable
{
	void *(*next)(const void *restrict node);
	void (*link)(void *restrict left, void *restrict right);
	_Bool (*leq)(const void *restrict left, const void *restrict right);
};

inline void *listsort(
	const struct listsort_vtable *restrict vtable, void *restrict head);

// HERE BE DRAGONS

inline void *listsort_merge_(
	const struct listsort_vtable *restrict vtable,
	void *restrict left, void *restrict right)
{
	void *head, *current, *other;
	if(vtable->leq(left, right))
	{
		head = current = left;
		other = right;
	}
	else
	{
		head = current = right;
		other = left;
	}

	for(void *next;;)
	{
		for(;; current = next)
		{
			next = vtable->next(current);
			if(!next)
			{
				vtable->link(current, other);
				return head;
			}

			if(!vtable->leq(next, other))
				break;
		}

		vtable->link(current, other);
		current = other;
		other = next;
	}
}

inline void *listsort(
	const struct listsort_vtable *restrict vtable, void *restrict head)
{
	if(!head) return NULL;

	void *stack[CHAR_BIT * sizeof (size_t)];
	size_t top = (size_t)-1;

	for(size_t count = 0; head; ++count)
	{
		stack[++top] = head;
		head = vtable->next(head);
		vtable->link(stack[top], NULL);
		for(size_t i = count; i % 2; i /= 2)
		{
			void *right = stack[top];
			void *left = stack[--top];
			stack[top] = listsort_merge_(vtable, left, right);
		}
	}

	while(top)
	{
		void *right = stack[top];
		void *left = stack[--top];
		stack[top] = listsort_merge_(vtable, left, right);
	}

	return *stack;
}

#endif
