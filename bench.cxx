#ifdef __cplusplus

#include <algorithm>
#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <forward_list>
#include <vector>

extern "C" unsigned bench_listsort(size_t count, const int values[]);

namespace {

	bool check(const std::forward_list<int>& list, int count) {
		int i = 0;
		for(std::forward_list<int>::const_iterator iter = list.begin();
			iter != list.end(); ++iter, ++i) {
			if(*iter != i)
				return 0;
		}

		return i == count;
	}

	unsigned bench_stl(std::vector<int>& values) {
		std::forward_list<int> list;
		for(std::vector<int>::size_type i = values.size(); i--;)
			list.push_front(values[i]);

		std::clock_t start = std::clock();
		list.sort();
		std::clock_t end = std::clock();

		assert(check(list, values.size()));
		return (unsigned)((end - start) * 1000 / CLOCKS_PER_SEC);
	}

	void do_bench(
		unsigned (&totals)[2], std::vector<int>& values, const int N) {
		unsigned times[] = { 0, 0 };

		for(int i = N; i-- > 0;) {
			times[0] += bench_listsort(values.size(), &values[0]);
			times[1] += bench_stl(values);
		};

		totals[0] += times[0];
		totals[1] += times[1];

		std::printf("listsort: %ums\nSTL: %ums\n\n", times[0], times[1]);
	}

}

int main() {
	const char *env_N = std::getenv("N");
	const char *env_SIZE = std::getenv("SIZE");
	if(!env_N || !env_SIZE) return EXIT_FAILURE;

	const int N = std::atoi(env_N);
	const int SIZE = std::atoi(env_SIZE);

	std::vector<int> values(SIZE);
	unsigned totals[] = { 0, 0 };

	puts("-- ascending --");
	for(int i = 0; i < SIZE; ++i) values[i] = i;
	do_bench(totals, values, N);

	std::puts("-- descending --");
	std::reverse(values.begin(), values.end());
	do_bench(totals, values, N);

	std::puts("-- random --");
	std::random_shuffle(values.begin(), values.end());
	do_bench(totals, values, N);

	std::puts("== total times ==");
	std::printf("listsort: %ums\nSTL: %ums\n", totals[0], totals[1]);

	return 0;
}

#else

#include "listsort.h"

#include <assert.h>
#include <stdlib.h>
#include <time.h>

struct node
{
	struct node *next;
	int value;
};

static void *next(const void *restrict node)
{
	return ((const struct node *)node)->next;
}

static void link(void *restrict left, void *restrict right)
{
	((struct node *)left)->next = right;
}

static _Bool leq(const void *restrict left, const void *restrict right)
{
	const struct node *left_node = left;
	const struct node *right_node = right;
	return left_node->value <= right_node->value;
}

static struct node *sort(struct node *head)
{
	static const struct listsort_vtable VTABLE = { next, link, leq };
	return listsort(&VTABLE, head);
}

static struct node *unshift(struct node *head, int value)
{
	struct node *node = malloc(sizeof *node);
	node->next = head;
	node->value = value;
	return node;
}

static _Bool check(const struct node *head, int count)
{
	int i = 0;
	for(; head; head = head->next, ++i)
	{
		if(head->value != i)
			return 0;
	}

	return i == count;
}

static void discard(struct node *head)
{
	for(struct node *next; head; head = next)
	{
		next = head->next;
		free(head);
	}
}

unsigned bench_listsort(size_t count, const int values[])
{
	struct node *head = NULL;
	for(size_t i = count; i--;)
		head = unshift(head, values[i]);

	clock_t start = clock();
	head = sort(head);
	clock_t end = clock();

	assert(check(head, count));
	discard(head);
	return (unsigned)((end - start) * 1000 / CLOCKS_PER_SEC);
}

#endif
