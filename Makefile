export N := 1
export SIZE := 500000

listsort.o : %.o : %.c %.h
	gcc -std=c99 -pedantic -Wall -Wextra -Werror -O2 -c -o $@ $<

bench.o : %.o : %.cxx listsort.h
	gcc -std=c99 -O3 -c -o $@ -x c $<

bench.exe : %.exe : %.cxx %.o listsort.o
	g++ -std=c++0x -O3 -o $@ $^

.PHONY : bench
bench : bench.exe
	./$<

.PHONY : clean
clean :; rm -rf listsort.o bench.o bench.exe
