/*	Copyright 2012 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#include "listsort.h"

extern void *listsort(
	const struct listsort_vtable *restrict vtable, void *restrict head);
